# DCPCryptCpp
Cryptographic Component Library DCPCrypt translated from Pascal to C++

Hashes from DCPCrypt:
- Haval
- MD4
- MD5
- Rimpemd128
- Rimpemd160
- Sha1-160
- Sha2-256

Updates:
Haval gives odd results, remained and renamed to HavalPas, add new Haval
Sha3 for 224,256,385 and 512 in one tiny, self explained but fast class.
not exists yet:
Sha2-512
Tiger

Ciphers from DCPCrypt:
- Blowfish
- Rijndael
- Serpent
- Twofish
not exists yet:
- Cast128
- Cast256
- DES
- Gost
- ICE
- Dea
- Mars
- Misty1
- RC2
- RC4
- RC5
- RC6
- Tea

Correct: reset index in Compress

Hash and cipher classes are low-level without stream handling etc.
Two test programs: testSpeed and hashFiles
testSpeed give with Visual Studio, Intel i3 3 GHz

Time of 100000 bytes
- ....
- md4 elapsed=147 us
- md5 elapsed=210 us
- sha1-160 elapsed=309 us
- sha2-256 elapsed=542 us
- sha3-224 elapsed=475 us
- sha3-256 elapsed=501 us <--very fast!
- sha3-384 elapsed=659 us
- sha3-512 elapsed=935 us

Time of 100000 bytes
 - blowfish : 899 us
 - rijndael : 506 us
 - serpent : 1324 us
 - twofish : 656 us

Argon2 and Blake2 from https://github.com/khovratovich/Argon2 
refcore.dat,optcore.dat - Argon2 benchmark on VS2019 , i3-4160 3.6 GHZ
-----
Program hashFiles uses mapping and Boost

