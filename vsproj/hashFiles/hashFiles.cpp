#include "DCPh_havalPas.h"
#include "DCPh_haval.h"
#include "DCPh_md4.h"
#include "DCPh_md5.h"
#include "DCPh_ripemd128.h"
#include "DCPh_ripemd160.h"
#include "DCPh_sha1_160.h"
#include "DCPh_sha2_256.h"
#include "DCPh_sha3.h"
#include "Exception.h"
#include <boost/interprocess/file_mapping.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <chrono>
#include <vector>
#include <algorithm>

using namespace std;
using namespace boost::interprocess;
using namespace dcpcrypt;

void fillHashVec(vector<DCP_hash*> &vec)
{
	for (int digestBits = 128; digestBits <= 256; digestBits += 32)
		for (int passCount = 3; passCount <= 5; passCount++)
		{
			if (digestBits / 64 != passCount - 1) continue;
			vec.push_back(new DCPh_havalPas(digestBits, passCount));
		}
	for (int digestBits = 128; digestBits <= 256; digestBits += 32)
		for (int passCount = 3; passCount <= 5; passCount++)
		{
			vec.push_back(new DCPh_haval(digestBits, passCount));
		}
	vec.push_back(new DCPh_md4());
	vec.push_back(new DCPh_md5());
	vec.push_back(new DCPh_ripemd128());
	vec.push_back(new DCPh_ripemd160());
	vec.push_back(new DCPh_sha1_160());
	vec.push_back(new DCPh_sha2_256());
	vec.push_back(new DCPh_sha3(256));
	vec.push_back(new DCPh_sha3(384));
	vec.push_back(new DCPh_sha3(512));
}

void freeHashVec(vector<DCP_hash*> &vec)
{
	for (size_t i = 0; i < vec.size(); i++)
		delete vec[i];
	vec.clear();
}

/*
void testHashesSpeed()
{

	fillHashVec(vec);
	const int size = 100000;
	const int loop = 1;
	const int ntrials = 100;
	printf("\nTime of %d bytes\n", size*loop);
	uint8_t *buf = new uint8_t[size];
	for (int i = 0; i < size; i++)
		buf[i] = (uint8_t)i;
	for (int i = 0; i < vec.size(); i++)
	{
		uint8_t digest[64];
		DCP_hash * hash = vec[i];
		bool selfTest = hash->selfTest();
		if (selfTest)
		{
			long long minElapsed = LLONG_MAX;
			for (int j = 0; j < ntrials; j++)
			{
				chrono::time_point<chrono::system_clock> start = chrono::system_clock::now();
				hash->init();
				for (int k = 0; k < loop; k++)
					hash->update(buf, size);
				hash->final(digest);
				auto current = chrono::system_clock::now();
				long long elapsed = chrono::duration_cast<chrono::microseconds>(current - start).count();
				if (elapsed < minElapsed)minElapsed = elapsed;
			}
			printf("%s elapsed=%lld us\n", hash->getAlgorithm().c_str(), minElapsed);
		}
		else printf("%s : selfTest error\n", hash->getAlgorithm());
	}
	delete buf;
	freeHashVec(vec);
}
*/
vector<DCP_hash*> vec;

void listinfo()
{
	for (int i = 0; i < vec.size(); i++)
	{
		DCP_hash * hash = vec[i];
		printf("%s\n", hash->getAlgorithm());
	}

}

void info(bool detailed)
{
	printf("use: hashfiles hashAlg pathtofile\n");
	printf("for example: hashfiles sha3-256 subdir/file.dat\n");
	if (detailed)
		listinfo();
	else printf("use: hashfiles --help for list of hashes");
}

DCP_hash * findHash(char *name)
{
	for (DCP_hash *hash : vec)
		if (hash->getAlgorithm() == string(name))
			return hash;
	return nullptr;
}


void usecToString(long long usec, char *buf)
{
	if (usec > 3600*1e6)
		sprintf(buf, "%.2fhours", usec / 3600.0 /1e6);
	else if (usec > 60*1e6)
		sprintf(buf, "%.2fmin", usec / 60.0 / 1e6);
	else if (usec > 1e6)
		sprintf(buf, "%.2fs", usec / 1e6);
	else if (usec > 1e3)
		sprintf(buf, "%.2f ms", usec / 1e3);
	else
		sprintf(buf, "%lld us", usec);
}

void processFile(DCP_hash *hash, const char *fileName)
{
	const size_t partSize = 100e6;
	printf("dot each %.0f MB\n", partSize / 1e6);
	chrono::time_point<chrono::system_clock> start = chrono::system_clock::now();
	try {
		file_mapping m_file(fileName, read_only);
		mapped_region region(m_file, read_only);
		unsigned char *addr = (unsigned char *)region.get_address();
		size_t size = region.get_size();
		hash->init();
		size_t processed = 0;
		while (processed < size)
		{
			size_t todo = min(partSize, size - processed);
			hash->update(addr+processed, todo);
			printf(".");
			processed += todo;
		}
		uint8_t digest[64];
		char buf[64];
		hash->final(digest);
		auto current = chrono::system_clock::now();
		long long elapsed = chrono::duration_cast<chrono::microseconds>(current - start).count();
		usecToString(elapsed, buf);
		printf("time %s\n", buf);
		printf("%s: ", hash->getAlgorithm());
		for (int i = 0; i < hash->getHashSize() / 8; i++)
		{
			printf("%02x", digest[i]);
		}
		printf("\n");
	}
	catch(...)
	{
		printf("can't open %s\n", fileName);
	}
}


int main(int argc, char * argv[])
{
	fillHashVec(vec);
	if (argc >= 3)
	{
		DCP_hash *hash = findHash(argv[1]);
		if (hash)
			processFile(hash, argv[2]);
		else info(true);
	}
	else if (argc == 2)
	{
		if (strcmp(argv[1], "--help") == 0)
			info(true);
		else
			info(false);
	}
	else info(false);
	freeHashVec(vec);
	return 0;
}

