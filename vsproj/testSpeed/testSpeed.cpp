#include "DCPh_havalPas.h"
#include "DCPh_haval.h"
#include "DCPh_md4.h"
#include "DCPh_md5.h"
#include "DCPh_ripemd128.h"
#include "DCPh_ripemd160.h"
#include "DCPh_sha1_160.h"
#include "DCPh_sha2_256.h"
#include "DCPh_sha3.h"
#include "DCPc_rijndael.h"
#include "DCPc_serpent.h"
#include "DCPc_blowfish.h"
#include "DCPc_twofish.h"
#include "Exception.h"
#include <chrono>
#include <vector>

using namespace dcpcrypt;

void fillHashVec(std::vector<DCP_hash*> &vec)
{
	for (int digestBits = 128; digestBits <= 256; digestBits += 32)
		for (int passCount = 3; passCount <= 5; passCount++)
		{
			if (digestBits / 64 != passCount - 1) continue;
			vec.push_back(new DCPh_havalPas(digestBits, passCount));
		}
	for (int digestBits = 128; digestBits <= 256; digestBits += 32)
		for (int passCount = 3; passCount <= 5; passCount++)
		{
			vec.push_back(new DCPh_haval(digestBits, passCount));
		}
	vec.push_back(new DCPh_md4());
	vec.push_back(new DCPh_md5());
	vec.push_back(new DCPh_ripemd128());
	vec.push_back(new DCPh_ripemd160());
	vec.push_back(new DCPh_sha1_160());
	vec.push_back(new DCPh_sha2_256());
	vec.push_back(new DCPh_sha3(224));
	vec.push_back(new DCPh_sha3(256));
	vec.push_back(new DCPh_sha3(384));
	vec.push_back(new DCPh_sha3(512));
}


void fillCipherVec(std::vector<DCP_cipher*> &vec)
{
	vec.push_back(new DCPc_blowfish());
	vec.push_back(new DCPc_rijndael());
	vec.push_back(new DCPc_serpent());
	vec.push_back(new DCPc_twofish());
}

void freeHashVec(std::vector<DCP_hash*> &vec)
{
	for (size_t i = 0; i < vec.size(); i++)
		delete vec[i];
	vec.clear();
}

void freeCipherVec(std::vector<DCP_cipher*> &vec)
{
	for (size_t i = 0; i < vec.size(); i++)
		delete vec[i];
	vec.clear();
}

void testHashesSpeed()
{
	std::vector<DCP_hash*> vec;
	fillHashVec(vec);
	const int size = 100000;
	const int loop = 1;
	const int ntrials = 100;
	printf("\nTime of %d bytes\n", size*loop);
	uint8_t *buf = new uint8_t[size];
	for (int i = 0; i < size; i++)
		buf[i] = (uint8_t)i;
	for (int i = 0; i < vec.size(); i++)
	{
		uint8_t digest[64];
		DCP_hash * hash = vec[i];
		bool selfTest = hash->selfTest();
		if (selfTest)
		{
			long long minElapsed = LLONG_MAX;
			for (int j = 0; j < ntrials; j++)
			{
				std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
				hash->init();
				for (int k = 0; k < loop; k++)
					hash->update(buf, size);
				hash->final(digest);
				auto current = std::chrono::system_clock::now();
				long long elapsed = std::chrono::duration_cast<std::chrono::microseconds>(current - start).count();
				if (elapsed < minElapsed)minElapsed = elapsed;
			}
			printf("%s elapsed=%lld us\n", hash->getAlgorithm().c_str(), minElapsed);
		}
		else printf("%s : selfTest error\n", hash->getAlgorithm().c_str());
	}
	delete buf;
	freeHashVec(vec);
}


void testCiphersSpeed()
{
	std::vector<DCP_cipher*> vec;
	fillCipherVec(vec);
	const int size = 100000;
	const int ntrials = 100;
	printf("\nTime of %d bytes\n", size);
	unsigned char* bufIn = new unsigned char[size];
	unsigned char* bufOut = new unsigned char[size];
	std::string strkey = "1234123412341234";
	memset(bufIn, 0xa0, size);
	for (int i = 0; i < vec.size(); i++)
	{
		DCP_cipher * cipher = vec[i];
		bool selfTest = cipher->selfTest();
		if (selfTest)
		{
			cipher->init((uint8_t*)strkey.c_str(), strkey.length() * 8, nullptr);
			long long minElapsed = LLONG_MAX;
			for (int j = 0; j < ntrials; j++)
			{
				std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
				((DCP_blockcipher*)cipher)->encryptCBC(bufIn, bufOut, size);
				auto current = std::chrono::system_clock::now();
				long long elapsed = std::chrono::duration_cast<std::chrono::microseconds>(current - start).count();
				if (elapsed < minElapsed)minElapsed = elapsed;
			}
			printf("%s : %lld us\n", cipher->getAlgorithm().c_str(), minElapsed);
		}
		else printf("%s : selfTest error\n", cipher->getAlgorithm().c_str());
	}
	delete bufIn;
	delete bufOut;
	freeCipherVec(vec);
}

int main(int argc, char * argv[])
{
	testHashesSpeed();
	testCiphersSpeed();
	return 0;
}

