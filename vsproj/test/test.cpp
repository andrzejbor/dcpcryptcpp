#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "DCPh_havalPas.h"
#include "DCPh_haval.h"
#include "DCPh_md4.h"
#include "DCPh_md5.h"
#include "DCPh_ripemd128.h"
#include "DCPh_ripemd160.h"
#include "DCPh_sha1_160.h"
#include "DCPh_sha2_256.h"
#include "DCPh_sha3.h"
#include "DCPc_rijndael.h"
#include "DCPc_serpent.h"
#include "DCPc_blowfish.h"
#include "DCPc_twofish.h"

using namespace dcpcrypt;

TEST_CASE("havalPas") {
	DCPh_havalPas hash;
	hash.selfTest();
}

TEST_CASE("haval") {
	DCPh_haval hash;
	hash.selfTest();
}

TEST_CASE("md4") {
	DCPh_md4 hash;
	hash.selfTest();
}

TEST_CASE("md5") {
	DCPh_md5 hash;
	hash.selfTest();
}

TEST_CASE("ripemd128") {
	DCPh_ripemd128 hash;
	hash.selfTest();
}

TEST_CASE("ripemd160") {
	DCPh_ripemd160 hash;
	hash.selfTest();
}

TEST_CASE("sha1_160") {
	DCPh_sha1_160 hash;
	hash.selfTest();
}

TEST_CASE("sha2_256") {
	DCPh_sha2_256 hash;
	hash.selfTest();
}

TEST_CASE("sha3") {
	DCPh_sha3 hash224(224);
	hash224.selfTest();
	DCPh_sha3 hash256(256);
	hash256.selfTest();
	DCPh_sha3 hash384(384);
	hash384.selfTest();
	DCPh_sha3 hash512(512);
	hash512.selfTest();
}

TEST_CASE("rijndael") {
	DCPc_rijndael crypt;
	crypt.selfTest();
}

TEST_CASE("serpent") {
	DCPc_serpent crypt;
	crypt.selfTest();
}

TEST_CASE("blowfish") {
	DCPc_blowfish crypt;
	crypt.selfTest();
}

TEST_CASE("twofish") {
	DCPc_twofish crypt;
	crypt.selfTest();
}
