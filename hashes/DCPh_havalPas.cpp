#include "DCPh_havalPas.h"
#include "Exception.h"

using namespace std;
using namespace dcpcrypt;

DCPh_havalPas::DCPh_havalPas(int digestBits, int passCount) :digestBits(digestBits), passCount(passCount)
{
}


DCPh_havalPas::~DCPh_havalPas()
{
};


void DCPh_havalPas::compress()
{
	uint32_t t7, t6, t5, t4, t3, t2, t1, t0;
	uint32_t W[32];
	uint32_t temp;
	memset(W, 0, sizeof(W));
	t0 = CurrentHash[0];
	t1 = CurrentHash[1];
	t2 = CurrentHash[2];
	t3 = CurrentHash[3];
	t4 = CurrentHash[4];
	t5 = CurrentHash[5];
	t6 = CurrentHash[6];
	t7 = CurrentHash[7];
	memmove(W, HashBuffer, sizeof(W));
	if (passCount == 3)
	{
#include "DCPh_havalPas3.h"
	}
	else if (passCount == 4)
	{
#include "DCPh_havalPas4.h"
	}
	else if (passCount == 5)
	{
#include "DCPh_havalPas5.h"
	}
	else throw EDCP_hash("Invalid pass count " + to_string(passCount));
	CurrentHash[0] += t0;
	CurrentHash[1] += t1;
	CurrentHash[2] += t2;
	CurrentHash[3] += t3;
	CurrentHash[4] += t4;
	CurrentHash[5] += t5;
	CurrentHash[6] += t6;
	CurrentHash[7] += t7;
	memset(W, 0, sizeof(W));
	index = 0;
	memset(HashBuffer, 0, sizeof(HashBuffer));
}


int DCPh_havalPas::getHashSize()
{
	return digestBits;
}


std::string DCPh_havalPas::getAlgorithm()
{
	string result = "havalPas-" + to_string(digestBits) + "-" + to_string(passCount);
    return result;
}


bool DCPh_havalPas::selfTest()
{
	bool result = false;
	if (passCount == 3)
	{
		if (digestBits == 128)
		{
			const uint8_t Test1Out[16] = { 0x1B, 0xDC, 0x55, 0x6B, 0x29, 0xAD, 0x02, 0xEC, 0x09, 0xAF, 0x8C, 0x66, 0x47, 0x7F, 0x2A, 0x87 };
			uint8_t TestOut[16];
			init();
			updateStr(string(""));
			final(TestOut);
			result = memcmp(TestOut, Test1Out, sizeof(Test1Out)) == 0;
		}
		else if (digestBits == 160)
		{
			const uint8_t Test1Out[20] = { 0x5E,0x16,0x10,0xFC,0xED,0x1D,0x3A,0xDB,0x0B,0xB1,
				0x8E,0x92,0xAC,0x2B,0x11,0xF0,0xBD,0x99,0xD8,0xED };
			uint8_t TestOut[20];
			init();
			updateStr(string("a"));
			final(TestOut);
			result = memcmp(TestOut, Test1Out, sizeof(Test1Out)) == 0;
		}
		else throw EDCP_hash("Bad combination len/numRounds");
	}
	else if (passCount == 4)
	{
		if (digestBits == 192)
		{
			const uint8_t Test1Out[24] = { 0x74,0xAA,0x31,0x18,0x2F,0xF0,0x9B,0xCC,0xE4,0x53,0xA7,0xF7,
				0x1B,0x5A,0x7C,0x5E,0x80,0x87,0x2F,0xA9,0x0C,0xD9,0x3A,0xE4 };
			uint8_t TestOut[24];
			init();
			updateStr(string("HAVAL"));
			final(TestOut);
			result = memcmp(TestOut, Test1Out, sizeof(Test1Out)) == 0;
		}
		else if (digestBits == 224)
		{
			const uint8_t Test1Out[28] = { 0x14,0x4C,0xB2,0xDE,0x11,0xF0,0x5D,0xF7,0xC3,0x56,0x28,0x2A,0x3B,0x48,
				0x57,0x96,0xDA,0x65,0x3F,0x6B,0x70,0x28,0x68,0xC7,0xDC,0xF4,0xAE,0x76 };
			uint8_t TestOut[28];
			init();
			updateStr(string("0123456789"));
			final(TestOut);
			result = memcmp(TestOut, Test1Out, sizeof(Test1Out)) == 0;
		}
		else throw EDCP_hash("Bad combination len/nuRounds");
	}
	else if (passCount == 5)
	{
		if (digestBits == 256)
		{
			const uint8_t Test1Out[32] = { 0x1A,0x1D,0xC8,0x09,0x9B,0xDA,0xA7,0xF3,0x5B,0x4D,0xA4,0xE8,0x05,0xF1,0xA2,0x8F,
				0xEE,0x90,0x9D,0x8D,0xEE,0x92,0x01,0x98,0x18,0x5C,0xBC,0xAE,0xD8,0xA1,0x0A,0x8D };
			const uint8_t Test2Out[32] = { 0xC5,0x64,0x7F,0xC6,0xC1,0x87,0x7F,0xFF,0x96,0x74,0x2F,0x27,0xE9,0x26,0x6B,0x68,
				0x74,0x89,0x4F,0x41,0xA0,0x8F,0x59,0x13,0x03,0x3D,0x9D,0x53,0x2A,0xED,0xDB,0x39 };
			uint8_t TestOut[32];
			init();
			updateStr(string("abcdefghijklmnopqrstuvwxyz"));
			final(TestOut);
			result = memcmp(TestOut, Test1Out, sizeof(Test1Out)) == 0;
			init();
			updateStr(string("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"));
			final(TestOut);
			result = memcmp(TestOut, Test2Out, sizeof(Test2Out)) == 0 && result;
		}
		else throw EDCP_hash("Bad combination len/nuRounds");
	}
	else throw EDCP_hash("Invalid pass count " + to_string(passCount));
	return result;
}

void DCPh_havalPas::init()
{
	burn();
	CurrentHash[0] = 0x243f6a88;
	CurrentHash[1] = 0x85a308d3;
	CurrentHash[2] = 0x13198a2e;
	CurrentHash[3] = 0x3707344;
	CurrentHash[4] = 0xa4093822;
	CurrentHash[5] = 0x299f31d0;
	CurrentHash[6] = 0x82efa98;
	CurrentHash[7] = 0xec4e6c89;
	fInitialized = true;
}

void DCPh_havalPas::burn()
{
	lenHi = 0; lenLo = 0;
	index = 0;
	memset(HashBuffer, 0, sizeof(HashBuffer));
	memset(CurrentHash, 0, sizeof(CurrentHash));
    fInitialized = false;
}

void DCPh_havalPas::update(const unsigned char *buffer, uint32_t size)
{
	const uint8_t *PBuf;
	if (!fInitialized)
		throw EDCP_hash("Hash not initialized");
	lenHi += size >> 29;
	lenLo += size * 8;
	if (lenLo < size * 8)
		lenHi++;

	PBuf = buffer;
	while (size > 0)
	{
		if (sizeof(HashBuffer) - index <= uint32_t(size))
		{
			memmove(&HashBuffer[index], PBuf, sizeof(HashBuffer) - index);
			size -= sizeof(HashBuffer) - index;
			PBuf += sizeof(HashBuffer) - index;
			compress();
		}
		else
		{
			memmove(&HashBuffer[index], PBuf, size);
			index += size;
			size = 0;
		}
	}
}

void DCPh_havalPas::final(uint8_t *digest)
{
	uint32_t temp;
	if (!fInitialized)
		throw EDCP_hash("Hash not initialized");
	HashBuffer[index] = 0x80;
	if (index >= 118)
		compress();
	if (passCount == 3)
	{
		if (digestBits == 128)
		{
			HashBuffer[118] = ((128 & 3) << 6) | (3 << 3) | 1;
			HashBuffer[119] = (128 >> 2) & 0xff;
		}
		else if (digestBits == 160)
		{
			HashBuffer[118] = ((160 & 3) << 6) | (3 << 3) | 1;
			HashBuffer[119] = (160 >> 2) & 0xff;
		}
		else if (digestBits == 192)
		{
			HashBuffer[118] = ((192 & 3) << 6) | (3 << 3) | 1;
			HashBuffer[119] = (192 >> 2) & 0xff;
		}
		else if (digestBits == 224)
		{
			HashBuffer[118] = ((224 & 3) << 6) | (3 << 3) | 1;
			HashBuffer[119] = (224 >> 2) & 0xff;
		}
		else if (digestBits == 256)
		{
			HashBuffer[118] = ((256 & 3) << 6) | (3 << 3) | 1;
			HashBuffer[119] = (256 >> 2) & 0xff;
		}
		else throw EDCP_hash("Invalid digestBits " + to_string(digestBits));
	}
	else if (passCount == 4)
	{
		if (digestBits == 128)
		{
			HashBuffer[118] = ((128 & 3) << 6) | (4 << 3) | 1;
			HashBuffer[119] = (128 >> 2) & 0xff;
		}
		else if (digestBits == 160)
		{
			HashBuffer[118] = ((160 & 3) << 6) | (4 << 3) | 1;
			HashBuffer[119] = (160 >> 2) & 0xff;
		}
		else if (digestBits == 192)
		{
			HashBuffer[118] = ((192 & 3) << 6) | (4 << 3) | 1;
			HashBuffer[119] = (192 >> 2) & 0xff;
		}
		else if (digestBits == 224)
		{
			HashBuffer[118] = ((224 & 3) << 6) | (4 << 3) | 1;
			HashBuffer[119] = (224 >> 2) & 0xff;
		}
		else if (digestBits == 256)
		{
			HashBuffer[118] = ((256 & 3) << 6) | (4 << 3) | 1;
			HashBuffer[119] = (256 >> 2) & 0xff;
		}
		else throw EDCP_hash("Invalid digestBits " + to_string(digestBits));
	}
	else if (passCount == 5)
	{
		if (digestBits == 128)
		{
			HashBuffer[118] = ((128 & 3) << 6) | (5 << 3) | 1;
			HashBuffer[119] = (2128 >> 2) & 0xff;
		}
		else if (digestBits == 160)
		{
			HashBuffer[118] = ((160 & 3) << 6) | (5 << 3) | 1;
			HashBuffer[119] = (160 >> 2) & 0xff;
		}
		else if (digestBits == 192)
		{
			HashBuffer[118] = ((192 & 3) << 6) | (5 << 3) | 1;
			HashBuffer[119] = (192 >> 2) & 0xff;
		}
		else if (digestBits == 224)
		{
			HashBuffer[118] = ((224 & 3) << 6) | (5 << 3) | 1;
			HashBuffer[119] = (224 >> 2) & 0xff;
		}
		else if (digestBits == 256)
		{
			HashBuffer[118] = ((256 & 3) << 6) | (5 << 3) | 1;
			HashBuffer[119] = (256 >> 2) & 0xff;
		}
		else throw EDCP_hash("Invalid digestBits " + to_string(digestBits));
	}
	else throw EDCP_hash("Invalid pass count " + to_string(passCount));

	*(uint32_t*)(HashBuffer+120) = lenLo;
	*(uint32_t*)(HashBuffer+124) = lenHi;
	compress();
	if (digestBits == 128)
	{
		temp = (CurrentHash[7] & 0xff) | (CurrentHash[6] & 0xff000000L) |
			(CurrentHash[5] & 0xff0000L) | (CurrentHash[4] & 0xff00L);
		CurrentHash[0] += (((unsigned long)temp) >> 8) | (temp << 24);
		temp = (CurrentHash[7] & 0xff00L) | (CurrentHash[6] & 0xff) |
			(CurrentHash[5] & 0xff000000L) | (CurrentHash[4] & 0xff0000L);
		CurrentHash[1] += (((unsigned long)temp) >> 16) | (temp << 16);
		temp = (CurrentHash[7] & 0xff0000L) | (CurrentHash[6] & 0xff00L) |
			(CurrentHash[5] & 0xff) | (CurrentHash[4] & 0xff000000L);
		CurrentHash[2] += (((unsigned long)temp) >> 24) | (temp << 8);
		temp = (CurrentHash[7] & 0xff000000L) | (CurrentHash[6] & 0xff0000L) |
			(CurrentHash[5] & 0xff00L) | (CurrentHash[4] & 0xff);
		CurrentHash[3] += temp;
		memmove(digest, CurrentHash, sizeof(long) * 4);
	}
	else if (digestBits == 160)
	{
		temp = (CurrentHash[7] & 0x3f) | (CurrentHash[6] & (0x7f << 25)) |
			(CurrentHash[5] & (0x3f << 19));
		CurrentHash[0] += (((unsigned long)temp) >> 19) | (temp << 13);
		temp = (CurrentHash[7] & (0x3f << 6)) | (CurrentHash[6] & 0x3f) |
			(CurrentHash[5] & (0x7f << 25));
		CurrentHash[1] += (((unsigned long)temp) >> 25) | (temp << 7);
		temp = (CurrentHash[7] & (0x7f << 12)) | (CurrentHash[6] & (0x3f << 6)) |
			(CurrentHash[5] & 0x3f);
		CurrentHash[2] += temp;
		temp = (CurrentHash[7] & (0x3f << 19)) | (CurrentHash[6] & (0x7f << 12)) |
			(CurrentHash[5] & (0x3f << 6));
		CurrentHash[3] += ((unsigned long)temp) >> 6;
		temp = (CurrentHash[7] & (0x7f << 25)) | (CurrentHash[6] & (0x3f << 19)) |
			(CurrentHash[5] & (0x7f << 12));
		CurrentHash[4] += ((unsigned long)temp) >> 12;
		memmove(digest, CurrentHash, sizeof(long) * 5);
	}
	else if (digestBits == 192)
	{
		temp = (CurrentHash[7] & 0x1f) | (CurrentHash[6] & (0x3f << 26));
		CurrentHash[0] += (((unsigned long)temp) >> 26) | (temp << 6);
		temp = (CurrentHash[7] & (0x1f << 5)) | (CurrentHash[6] & 0x1f);
		CurrentHash[1] += temp;
		temp = (CurrentHash[7] & (0x3f << 10)) | (CurrentHash[6] & (0x1f << 5));
		CurrentHash[2] += ((unsigned long)temp) >> 5;
		temp = (CurrentHash[7] & (0x1f << 16)) | (CurrentHash[6] & (0x3f << 10));
		CurrentHash[3] += ((unsigned long)temp) >> 10;
		temp = (CurrentHash[7] & (0x1f << 21)) | (CurrentHash[6] & (0x1f << 16));
		CurrentHash[4] += ((unsigned long)temp) >> 16;
		temp = (CurrentHash[7] & (0x3f << 26)) | (CurrentHash[6] & (0x1f << 21));
		CurrentHash[5] += ((unsigned long)temp) >> 21;
		memmove(digest, CurrentHash, sizeof(long) * 6);
	}
	else if (digestBits == 224)
	{
		CurrentHash[0] += (((unsigned long)CurrentHash[7]) >> 27) & 0x1f;
		CurrentHash[1] += (((unsigned long)CurrentHash[7]) >> 22) & 0x1f;
		CurrentHash[2] += (((unsigned long)CurrentHash[7]) >> 18) & 0xf;
		CurrentHash[3] += (((unsigned long)CurrentHash[7]) >> 13) & 0x1f;
		CurrentHash[4] += (((unsigned long)CurrentHash[7]) >> 9) & 0xf;
		CurrentHash[5] += (((unsigned long)CurrentHash[7]) >> 4) & 0x1f;
		CurrentHash[6] += CurrentHash[7] & 0xf;
		memmove(digest, CurrentHash, sizeof(long) * 7);
	}
	else if (digestBits == 256)
	{
		memmove(digest, CurrentHash, sizeof(long) * 8);
	}
	else throw EDCP_hash("Invalid digestBits " + to_string(digestBits));
	burn();
}
